package lut.star.com.srs.customViews.chart.interfaces.dataprovider;

import lut.star.com.srs.customViews.chart.components.YAxis;
import lut.star.com.srs.customViews.chart.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
