package lut.star.com.srs.customViews.tableview.sort;

/**
 * Created by starlut on 11/09/2018.
 */

public interface ISortableModel {

    String getId();

    Object getContent();
}
