package lut.star.com.srs.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataOne {
    @SerializedName("wilcon_data")
    @Expose
    public List<DataOneModel> dataOneModels;
}
