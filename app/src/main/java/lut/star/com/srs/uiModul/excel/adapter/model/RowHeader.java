package lut.star.com.srs.uiModul.excel.adapter.model;

public class RowHeader extends Cell {

    public RowHeader(String id) {
        super(id);
    }

    public RowHeader(String id, String data) {
        super(id, data);
    }
}

