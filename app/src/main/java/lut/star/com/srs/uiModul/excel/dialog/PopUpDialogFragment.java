package lut.star.com.srs.uiModul.excel.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lut.star.com.srs.R;

public class PopUpDialogFragment extends DialogFragment {
    @BindView(R.id.spLeft) public Spinner spLeft;
    @BindView(R.id.spBottom) public Spinner spBottom;
    @BindView(R.id.spTop) public Spinner spTop;
    @BindView(R.id.rvFilter) public RecyclerView rvFilter;

    private List<String> column = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.sheet_configure_pop_up, null);
        builder.setView(view);

        ButterKnife.bind(view);

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PopUpDialogFragment.this.getDialog().cancel();
            }
        });

        spLeft = (Spinner) view.findViewById(R.id.spLeft);
        spTop = (Spinner) view.findViewById(R.id.spTop);
        spBottom = (Spinner) view.findViewById(R.id.spBottom);
        rvFilter = (RecyclerView) view.findViewById(R.id.rvFilter);

        initView();
        return builder.create();
    }

    private void initView() {
        List<String> mColumnHeaderList = new ArrayList<>();
        mColumnHeaderList.add( "Artikel Belong");
        mColumnHeaderList.add( "Open Order");
        mColumnHeaderList.add( "Zukunft");
        mColumnHeaderList.add( "Nov17");
        mColumnHeaderList.add( "Dez17");
        mColumnHeaderList.add( "Jan18");
        mColumnHeaderList.add( "Feb18");
        mColumnHeaderList.add( "Mrz18");
        mColumnHeaderList.add( "Apr18");
        mColumnHeaderList.add( "Mai18");
        mColumnHeaderList.add( "Jun18");
        mColumnHeaderList.add( "Jul18");
        mColumnHeaderList.add( "Aug18");
        mColumnHeaderList.add( "total");

        ArrayAdapter adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_expandable_list_item_1, mColumnHeaderList);
        spBottom.setAdapter(adapter);
        spTop.setAdapter(adapter);
        spLeft.setAdapter(adapter);

        rvFilter.setHasFixedSize(true);
        rvFilter.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        FilterRvAdapter adapter2 = new FilterRvAdapter(mColumnHeaderList, new FilterRvAdapter.ItemListener() {
            @Override
            public void onItemClick(String item) {

            }
        });
        rvFilter.setAdapter(adapter2);
    }
}
