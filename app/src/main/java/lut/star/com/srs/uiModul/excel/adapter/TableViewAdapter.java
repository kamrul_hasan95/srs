package lut.star.com.srs.uiModul.excel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import lut.star.com.srs.customViews.tableview.adapter.AbstractTableAdapter;
import lut.star.com.srs.customViews.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import java.util.List;

import lut.star.com.srs.R;
import lut.star.com.srs.uiModul.excel.adapter.model.Cell;
import lut.star.com.srs.uiModul.excel.adapter.model.ColumnHeader;
import lut.star.com.srs.uiModul.excel.adapter.model.RowHeader;

public class TableViewAdapter extends AbstractTableAdapter<ColumnHeader, RowHeader, Cell> {
    private Context context;
    private int BUTTON_VIEW = 1;
    List<ColumnHeader> columnHeaders;
    List<RowHeader> rowHeaders;
    List<List<Cell>> cells;

    public void setAllITemsList(List<ColumnHeader> columnHeaders , List<RowHeader> rowHeaders, List<List<Cell>> cells){
        this.columnHeaders = columnHeaders;
        this.rowHeaders = rowHeaders;
        this.cells = cells;
    }

    public TableViewAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public int getColumnHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getRowHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCellItemViewType(int position) {
        if (position == mColumnHeaderItems.size() - 1)
            return BUTTON_VIEW;
        else
            return 0;
    }

    class MyCellViewHolder extends AbstractViewHolder {

        public final TextView cell_textview;

        public MyCellViewHolder(View itemView) {
            super(itemView);
            cell_textview = (TextView) itemView.findViewById(R.id.tvCell);
        }
    }

    class MyCellButtonViewHolder extends AbstractViewHolder{
        public final TextView button;
        public MyCellButtonViewHolder(View itemView) {
            super(itemView);

            button = (TextView) itemView.findViewById(R.id.btnGraph);
        }
    }

    @Override
    public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
        // Get cell xml layoutif
        if (viewType == BUTTON_VIEW){
            View layout = LayoutInflater.from(context).inflate(R.layout.table_view_graph_button,
                    parent, false);
            return new MyCellButtonViewHolder(layout);
        } else {
            View layout = LayoutInflater.from(context).inflate(R.layout.my_cell_layout,
                    parent, false);
            // Create a Custom ViewHolder for a Cell item.
            return new MyCellViewHolder(layout);
        }
    }

    @Override
    public void onBindCellViewHolder(AbstractViewHolder holder, Object cellItemModel, int columnPosition, final int rowPosition) {
        Cell cell = (Cell) cellItemModel;
        if (holder instanceof MyCellButtonViewHolder){
            MyCellButtonViewHolder viewHolder = (MyCellButtonViewHolder) holder;
        }else {
            MyCellViewHolder viewHolder = (MyCellViewHolder) holder;
            viewHolder.cell_textview.setText(String.valueOf(cell.getData()));
        }
    }

    class MyColumnHeaderViewHolder extends AbstractViewHolder {

        public final TextView cell_textview;

        public MyColumnHeaderViewHolder(View itemView) {
            super(itemView);
            cell_textview = (TextView) itemView.findViewById(R.id.tvRowHeader);
        }
    }

    @Override
    public AbstractViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(context).inflate(R.layout
                .table_view_row_header, parent, false);

        // Create a ColumnHeader ViewHolder
        return new MyColumnHeaderViewHolder(layout);
    }

    @Override
    public void onBindColumnHeaderViewHolder(AbstractViewHolder holder, Object columnHeaderItemModel, int columnPosition) {
        ColumnHeader columnHeader = (ColumnHeader) columnHeaderItemModel;

        // Get the holder to update cell item text
        MyColumnHeaderViewHolder columnHeaderViewHolder = (MyColumnHeaderViewHolder) holder;
        columnHeaderViewHolder.cell_textview.setText(String.valueOf(columnHeader.getData()));
    }

    class MyRowHeaderViewHolder extends AbstractViewHolder {

        public final TextView cell_textview;

        public MyRowHeaderViewHolder(View itemView) {
            super(itemView);
            cell_textview = (TextView) itemView.findViewById(R.id.tvRowHeader);
//            cell_textview.setWidth((int)itemView.getResources().getDimension(R.dimen.default_row_header_width));
        }
    }

    @Override
    public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(context).inflate(R.layout
                .table_view_row_header, parent, false);

        // Create a Row Header ViewHolder
        return new MyRowHeaderViewHolder(layout);
    }

    @Override
    public void onBindRowHeaderViewHolder(AbstractViewHolder holder, Object rowHeaderItemModel, int rowPosition) {
        RowHeader rowHeader = (RowHeader) rowHeaderItemModel;

        // Get the holder to update row header item text
        MyRowHeaderViewHolder rowHeaderViewHolder = (MyRowHeaderViewHolder) holder;
        rowHeaderViewHolder.cell_textview.setText(String.valueOf(rowPosition));
    }

    @Override
    public View onCreateCornerView() {
        // Get Corner xml layout
        return LayoutInflater.from(mContext).inflate(R.layout.table_view_corner_layout, null);
    }
}
