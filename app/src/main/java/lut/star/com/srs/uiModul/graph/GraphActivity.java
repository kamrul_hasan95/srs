package lut.star.com.srs.uiModul.graph;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import lut.star.com.srs.constants.ValueConstants;
import lut.star.com.srs.customViews.chart.charts.BarChart;
import lut.star.com.srs.customViews.chart.components.Description;
import lut.star.com.srs.customViews.chart.components.XAxis;
import lut.star.com.srs.customViews.chart.data.BarData;
import lut.star.com.srs.customViews.chart.data.BarDataSet;
import lut.star.com.srs.customViews.chart.data.BarEntry;
import lut.star.com.srs.customViews.chart.formatter.IndexAxisValueFormatter;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lut.star.com.srs.R;
import lut.star.com.srs.base.BaseActivity;
import lut.star.com.srs.datamodel.DataOneModel;

public class GraphActivity extends BaseActivity implements GraphViewInterface{
    private GraphPresenter presenter;

    @BindView(R.id.tvTitle) public TextView tvTitle;
    @BindView(R.id.tvTotal) public TextView tvTotal;
    @BindView(R.id.bargraph) public BarChart barChart;
    
    
    private int selectedGraph;
    private List<DataOneModel> list;
    private ArrayList<String> date;
    private ArrayList<BarEntry> barEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        selectedGraph = extras.getInt(ValueConstants.SELECTED_GRAPH_ID);

        presenter = new GraphPresenter(this, this);
        presenter.getDataFromJson();
    }

    @Override
    public void onDataFetched(List<DataOneModel> dataOneModels) {
        list = dataOneModels;

        initView();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_graph;
    }
    
    private void initView(){
        try {
            tvTitle.setText(list.get(selectedGraph).artikelBelong);
        }catch (NullPointerException n){
            tvTitle.setVisibility(View.INVISIBLE);
            n.printStackTrace();
        }
        try {
            tvTotal.setText(list.get(selectedGraph).total);
        }catch (NullPointerException n){
            tvTotal.setVisibility(View.INVISIBLE);
            n.printStackTrace();
        }


        initGraph();
    }

    private void initGraph(){
        date = new ArrayList<>();
        date.add(new String("Open Order"));
        date.add(new String("Zukunft"));
        date.add(new String("Nov 17"));
        date.add(new String("Dec 17"));
        date.add(new String("Jan 18"));
        date.add(new String("Feb 18"));
        date.add(new String("Mar 18"));
        date.add(new String("Apr 18"));
        date.add(new String("May 18"));
        date.add(new String("Jun 18"));
        date.add(new String("Jul 18"));
        date.add(new String("Aug 18"));
//        date.add(new String("Total"));
        
        barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(0f, Float.parseFloat(list.get(selectedGraph).openOrder), date.get(0)));
        barEntries.add(new BarEntry(1f, Float.parseFloat(list.get(selectedGraph).zukunft), date.get(1)));
        barEntries.add(new BarEntry(2f, Float.parseFloat(list.get(selectedGraph).nov17), date.get(2)));
        barEntries.add(new BarEntry(3f, Float.parseFloat(list.get(selectedGraph).dez17), date.get(3)));
        barEntries.add(new BarEntry(4f, Float.parseFloat(list.get(selectedGraph).jan18), date.get(4)));
        barEntries.add(new BarEntry(5f, Float.parseFloat(list.get(selectedGraph).feb18), date.get(5)));
        barEntries.add(new BarEntry(6f, Float.parseFloat(list.get(selectedGraph).mrz18), date.get(6)));
        barEntries.add(new BarEntry(7f, Float.parseFloat(list.get(selectedGraph).apr18), date.get(7)));
        barEntries.add(new BarEntry(8f, Float.parseFloat(list.get(selectedGraph).mai18), date.get(8)));
        barEntries.add(new BarEntry(9f, Float.parseFloat(list.get(selectedGraph).jun18), date.get(9)));
        barEntries.add(new BarEntry(10f, Float.parseFloat(list.get(selectedGraph).jul18), date.get(10)));
        barEntries.add(new BarEntry(11f, Float.parseFloat(list.get(selectedGraph).aug18), date.get(11)));
//        barEntries.add(new BarEntry(12f, Float.parseFloat(list.get(selectedGraph).total), date.get(12)));

        BarDataSet barDataSet = new BarDataSet(barEntries,"WIlcon");
        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);
        Description description = new Description();
        description.setText("Courses");
        barChart.setDescription(description);

        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.invalidate(); // refresh
        barChart.setPinchZoom(true); //zoom feature
        barChart.setDrawValueAboveBar(true);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(date));
    }
}
