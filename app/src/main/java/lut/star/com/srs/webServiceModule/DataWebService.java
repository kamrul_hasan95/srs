package lut.star.com.srs.webServiceModule;

import io.reactivex.Observable;
import lut.star.com.srs.datamodel.DataOne;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by kamrulhasan on 18/9/18.
 */
public interface DataWebService {

    @GET("/sls_demo")
    Observable<Response<DataOne>> getAgendaListDay1();
}
