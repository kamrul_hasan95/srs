package lut.star.com.srs.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        //for remote crash log
        Fabric.with(this, new Crashlytics());

        //bind view here for all activities extending this Activity
        ButterKnife.bind(this);
    }


    /**
     * get layout to inflate
     */
    public abstract
    @LayoutRes
    int getLayout();
}
