
package lut.star.com.srs.uiModul.excel.dialog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.List;

import lut.star.com.srs.R;


public class FilterRvAdapter extends RecyclerView.Adapter<FilterRvAdapter.ViewHolder> {

    private List<String> myItems;
    private ItemListener myListener;

    public FilterRvAdapter(List<String> items, ItemListener listener) {
        myItems = items;
        myListener = listener;
    }

    public void setListener(ItemListener listener) {
        myListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_column_filter, parent, false)); // TODO
    }

    @Override
    public int getItemCount() {
        return myItems.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(myItems.get(position));
    }

    public interface ItemListener {
        void onItemClick(String item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // TODO - Your view members
        public String item;
        public CheckBox cbFilterItem;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            // TODO instantiate/assign view members

            cbFilterItem = (CheckBox)itemView.findViewById(R.id.cbColumnFilter);
        }

        public void setData(String item) {
            this.item = item;
            // TODO set data to view
            cbFilterItem.setText(item);
        }

        @Override
        public void onClick(View v) {
            if (myListener != null) {
                myListener.onItemClick(item);
            }
        }
    }


}
                                