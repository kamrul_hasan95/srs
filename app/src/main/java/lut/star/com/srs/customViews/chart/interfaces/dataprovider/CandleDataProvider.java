package lut.star.com.srs.customViews.chart.interfaces.dataprovider;

import lut.star.com.srs.customViews.chart.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    CandleData getCandleData();
}
