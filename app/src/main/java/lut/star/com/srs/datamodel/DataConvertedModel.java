package lut.star.com.srs.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamrulhasan on 18/9/18.
 */
public class DataConvertedModel {
    @SerializedName("Artikel_Belong")
    @Expose
    public String artikelBelong;
    @SerializedName("Open Order")
    @Expose
    public Double openOrder;
    @SerializedName("Zukunft")
    @Expose
    public Double zukunft;
    @SerializedName("Nov.17")
    @Expose
    public Double nov17;
    @SerializedName("Dez.17")
    @Expose
    public Double dez17;
    @SerializedName("Jan.18")
    @Expose
    public Double jan18;
    @SerializedName("Feb.18")
    @Expose
    public Double feb18;
    @SerializedName("Mrz.18")
    @Expose
    public Double mrz18;
    @SerializedName("Apr.18")
    @Expose
    public Double apr18;
    @SerializedName("Mai.18")
    @Expose
    public Double mai18;
    @SerializedName("Jun.18")
    @Expose
    public Double jun18;
    @SerializedName("Jul.18")
    @Expose
    public Double jul18;
    @SerializedName("Aug.18")
    @Expose
    public Double aug18;
    @SerializedName("Total")
    @Expose
    public Double total;

    @Override
    public String toString() {
        return "DataOneModel{" +
                "artikelBelong='" + artikelBelong + '\'' +
                ", openOrder='" + openOrder + '\'' +
                ", zukunft='" + zukunft + '\'' +
                ", nov17='" + nov17 + '\'' +
                ", dez17='" + dez17 + '\'' +
                ", jan18='" + jan18 + '\'' +
                ", feb18='" + feb18 + '\'' +
                ", mrz18='" + mrz18 + '\'' +
                ", apr18='" + apr18 + '\'' +
                ", mai18='" + mai18 + '\'' +
                ", jun18='" + jun18 + '\'' +
                ", jul18='" + jul18 + '\'' +
                ", aug18='" + aug18 + '\'' +
                ", total='" + total + '\'' +
                '}';
    }
}
