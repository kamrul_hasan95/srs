package lut.star.com.srs.uiModul.graph;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import lut.star.com.srs.R;
import lut.star.com.srs.datamodel.DataOne;

public class GraphPresenter {
    private Context context;
    private GraphViewInterface viewInterface;

    public GraphPresenter(Context context, GraphViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void getDataFromJson(){
        InputStream in = context.getResources().openRawResource(R.raw.xcelone);
        Reader reader = new BufferedReader(new InputStreamReader(in));
        Gson gson = new Gson();
        DataOne dataOne = gson.fromJson(reader, DataOne.class);
        viewInterface.onDataFetched(dataOne.dataOneModels);
    }
}
