package lut.star.com.srs.customViews.chart.interfaces.dataprovider;

import lut.star.com.srs.customViews.chart.components.YAxis.AxisDependency;
import lut.star.com.srs.customViews.chart.data.BarLineScatterCandleBubbleData;
import lut.star.com.srs.customViews.chart.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(AxisDependency axis);
    boolean isInverted(AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
