package lut.star.com.srs.uiModul.excel;

import java.util.List;

import lut.star.com.srs.datamodel.DataConvertedModel;
import lut.star.com.srs.datamodel.DataOneModel;

public interface ExcelViewInterface {
    void onDataFetched(List<DataOneModel> models);
}
