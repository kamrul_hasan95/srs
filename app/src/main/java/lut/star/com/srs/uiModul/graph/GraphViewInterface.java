package lut.star.com.srs.uiModul.graph;

import java.util.List;

import lut.star.com.srs.datamodel.DataOneModel;

public interface GraphViewInterface {
    void onDataFetched(List<DataOneModel> dataOneModels);
}
