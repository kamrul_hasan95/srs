package lut.star.com.srs.base;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;

import lut.star.com.srs.BuildConfig;
import lut.star.com.srs.constants.ValueConstants;
import timber.log.Timber;

public class BaseApplication extends Application {
//    private DaoSession mDaoSession;
    private static BaseApplication instance;

    public void onCreate() {
        super.onCreate();

        instance = this;
        //green dao
//        mDaoSession = new DaoMaster(
//                new DaoMaster.DevOpenHelper(this, ValueConstants.DATABASE_DIA).getWritableDatabase()).newSession();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

//    public DaoSession getDaoSession() {
//        return mDaoSession;
//    }

    public static BaseApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
