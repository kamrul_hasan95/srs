package lut.star.com.srs.customViews.tableview;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import lut.star.com.srs.customViews.tableview.adapter.AbstractTableAdapter;
import lut.star.com.srs.customViews.tableview.adapter.recyclerview.CellRecyclerView;
import lut.star.com.srs.customViews.tableview.filter.Filter;
import lut.star.com.srs.customViews.tableview.handler.FilterHandler;
import lut.star.com.srs.customViews.tableview.handler.SelectionHandler;
import lut.star.com.srs.customViews.tableview.layoutmanager.CellLayoutManager;
import lut.star.com.srs.customViews.tableview.layoutmanager.ColumnHeaderLayoutManager;
import lut.star.com.srs.customViews.tableview.listener.ITableViewListener;
import lut.star.com.srs.customViews.tableview.listener.scroll.HorizontalRecyclerViewListener;
import lut.star.com.srs.customViews.tableview.listener.scroll.VerticalRecyclerViewListener;
import lut.star.com.srs.customViews.tableview.sort.SortState;

/**
 * Created by starlut on 11/09/2018.
 */

public interface ITableView {

    void addView(View child, ViewGroup.LayoutParams params);

    boolean hasFixedWidth();

    boolean isIgnoreSelectionColors();

    boolean isShowHorizontalSeparators();

    boolean isSortable();

    CellRecyclerView getCellRecyclerView();

    CellRecyclerView getColumnHeaderRecyclerView();

    CellRecyclerView getRowHeaderRecyclerView();

    ColumnHeaderLayoutManager getColumnHeaderLayoutManager();

    CellLayoutManager getCellLayoutManager();

    LinearLayoutManager getRowHeaderLayoutManager();

    HorizontalRecyclerViewListener getHorizontalRecyclerViewListener();

    VerticalRecyclerViewListener getVerticalRecyclerViewListener();

    ITableViewListener getTableViewListener();

    SelectionHandler getSelectionHandler();

    DividerItemDecoration getHorizontalItemDecoration();

    SortState getSortingStatus(int column);

    void scrollToColumnPosition(int column);

    void scrollToRowPosition(int row);

    void showRow(int row);

    void hideRow(int row);

    boolean isRowVisible(int row);

    void showAllHiddenRows();

    void clearHiddenRowList();

    void showColumn(int column);

    void hideColumn(int column);

    boolean isColumnVisible(int column);

    void showAllHiddenColumns();

    void clearHiddenColumnList();

    int getShadowColor();

    int getSelectedColor();

    int getUnSelectedColor();

    void sortColumn(int columnPosition, SortState sortState);

    void remeasureColumnWidth(int column);

    AbstractTableAdapter getAdapter();

    /**
     * Filters the whole table using the provided Filter object which supports multiple filters.
     *
     * @param filter The filter object.
     */
    void filter(Filter filter);

    /**
     * Retrieves the FilterHandler of the TableView.
     *
     * @return The FilterHandler of the TableView.
     */
    FilterHandler getFilterHandler();
}
