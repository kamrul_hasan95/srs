package lut.star.com.srs.uiModul.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.OnClick;
import lut.star.com.srs.R;
import lut.star.com.srs.base.BaseActivity;
import lut.star.com.srs.uiModul.excel.ExcelActivity;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_home;
    }

    @OnClick(R.id.btnExcell)
    public void goToExcell(){
        Intent intent = new Intent(this, ExcelActivity.class);
        startActivity(intent);
    }
}
