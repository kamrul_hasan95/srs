package lut.star.com.srs.customViews.chart.interfaces.dataprovider;

import lut.star.com.srs.customViews.chart.data.CombinedData;

public interface CombinedDataProvider extends LineDataProvider, BarDataProvider, BubbleDataProvider, CandleDataProvider, ScatterDataProvider {

    CombinedData getCombinedData();
}
