package lut.star.com.srs.uiModul.excel;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lut.star.com.srs.R;
import lut.star.com.srs.base.BaseActivity;
import lut.star.com.srs.constants.ValueConstants;
import lut.star.com.srs.customViews.tableview.TableView;
import lut.star.com.srs.customViews.tableview.listener.ITableViewListener;
import lut.star.com.srs.customViews.tableview.sort.SortState;
import lut.star.com.srs.datamodel.DataOneModel;
import lut.star.com.srs.uiModul.excel.adapter.TableViewAdapter;
import lut.star.com.srs.uiModul.excel.adapter.model.Cell;
import lut.star.com.srs.uiModul.excel.adapter.model.ColumnHeader;
import lut.star.com.srs.uiModul.excel.adapter.model.RowHeader;
import lut.star.com.srs.uiModul.excel.dialog.PopUpDialogFragment;
import lut.star.com.srs.uiModul.graph.GraphActivity;
import timber.log.Timber;

public class ExcelActivity extends BaseActivity implements ExcelViewInterface {
    private String TAG = this.getClass().getSimpleName();
    private ExcellPresenter excellPresenter;

    List<DataOneModel> models;

    private List<RowHeader> mRowHeaderList;
    private List<ColumnHeader> mColumnHeaderList;
    private List<List<Cell>> mCellList;
    private TableViewAdapter tableViewAdapter;
    private SortState sortState = SortState.UNSORTED;
    private int ACTION_TOTAL = 0, ACTION_GRAPH = 1, action = -1;

    @BindView(R.id.tableView) public TableView tableView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        excellPresenter = new ExcellPresenter(this, this);

        Timber.d("oncreate of act");

        excellPresenter.getDataFromWebService();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_excel;
    }

    @Override
    public void onDataFetched(List<DataOneModel> models) {
        this.models = models;

        initTable();
    }

    public void init(){
        initRowHeader();
        initColumnHeader();
        initCell();


    }

    private void initCell() {
        mCellList = new ArrayList<>();
        mCellList.clear();
        for (int i = 0 ; i < models.size() ; i++){
            List<Cell> cells = new ArrayList<Cell>();
            cells.add(new Cell("0"+i,models.get(i).artikelBelong));
            cells.add(new Cell("1"+i, models.get(i).openOrder));
            cells.add(new Cell("2"+i, models.get(i).zukunft));
            cells.add(new Cell("3"+i, models.get(i).nov17));
            cells.add(new Cell("4"+i, models.get(i).dez17));
            cells.add(new Cell("5"+i, models.get(i).jan18));
            cells.add(new Cell("6"+i, models.get(i).feb18));
            cells.add(new Cell("7"+i, models.get(i).mrz18));
            cells.add(new Cell("8"+i, models.get(i).apr18));
            cells.add(new Cell("9"+i, models.get(i).mai18));
            cells.add(new Cell("10"+i, models.get(i).jun18));
            cells.add(new Cell("11"+i, models.get(i).jul18));
            cells.add(new Cell("12"+i, models.get(i).aug18));
            cells.add(new Cell("13"+i, models.get(i).total));
            cells.add(new Cell("14"+i, "Go to graph"));
            mCellList.add(cells);
        }

//        int i = models.size();
//        List<String> strings = getTotal();
//        List<Cell> cells = new ArrayList<>();
//        cells.add(new Cell("0"+i,"Total"));
//        cells.add(new Cell("1"+i,strings.get(0)));
//        cells.add(new Cell("2"+i,strings.get(1)));
//        cells.add(new Cell("3"+i,strings.get(2)));
//        cells.add(new Cell("4"+i,strings.get(3)));
//        cells.add(new Cell("5"+i,strings.get(4)));
//        cells.add(new Cell("6"+i,strings.get(5)));
//        cells.add(new Cell("7"+i,strings.get(6)));
//        cells.add(new Cell("8"+i,strings.get(7)));
//        cells.add(new Cell("9"+i,strings.get(8)));
//        cells.add(new Cell("10"+i,strings.get(9)));
//        cells.add(new Cell("11"+i,strings.get(10)));
//        cells.add(new Cell("12"+i,strings.get(11)));
//        cells.add(new Cell("13"+i,strings.get(12)));
//        cells.add(new Cell("14"+i, ""));
//
//        mCellList.add(cells);
    }

    private void initColumnHeader() {
        mColumnHeaderList = new ArrayList<>();
        mColumnHeaderList.clear();
        mColumnHeaderList.add(new ColumnHeader("0", "Artikel Belong"));
        mColumnHeaderList.add(new ColumnHeader("1", "Open Order"));
        mColumnHeaderList.add(new ColumnHeader("2", "Zukunft"));
        mColumnHeaderList.add(new ColumnHeader("3", "Nov17"));
        mColumnHeaderList.add(new ColumnHeader("4", "Dez17"));
        mColumnHeaderList.add(new ColumnHeader("5", "Jan18"));
        mColumnHeaderList.add(new ColumnHeader("6", "Feb18"));
        mColumnHeaderList.add(new ColumnHeader("7", "Mrz18"));
        mColumnHeaderList.add(new ColumnHeader("8", "Apr18"));
        mColumnHeaderList.add(new ColumnHeader("9", "Mai18"));
        mColumnHeaderList.add(new ColumnHeader("10", "Jun18"));
        mColumnHeaderList.add(new ColumnHeader("11", "Jul18"));
        mColumnHeaderList.add(new ColumnHeader("12", "Aug18"));
        mColumnHeaderList.add(new ColumnHeader("13", "total"));
        mColumnHeaderList.add(new ColumnHeader("14", " "));
    }

//    private List<String> getTotal(){
//        List<String> totals = new ArrayList<>();
//        double openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).openOrder;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).zukunft;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).nov17;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).dez17;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).jan18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).feb18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).mai18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).apr18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).mai18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).jun18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).jul18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).aug18;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//        openOrderDouble = 0;
//        for (int i = 0 ; i < models.size() ; i++){
//            openOrderDouble = openOrderDouble +  models.get(i).total;
//        }
//        totals.add(String.valueOf(new DecimalFormat("0.###").format(openOrderDouble)));
//
//        return totals;
//    }

    private void initRowHeader() {
        mRowHeaderList = new ArrayList<>();
        mRowHeaderList.clear();

        for (int i = 0 ; i <= models.size() ; i++){
            mRowHeaderList.add(new RowHeader(""+i, ""+i));
        }
    }

    private void initTable(){

        // Set adapter
        tableViewAdapter = new TableViewAdapter(this);


        tableView.setAdapter(tableViewAdapter);


        init();

        tableViewAdapter.setTableView(tableView);
        tableViewAdapter.setRowHeaderWidth((int)getResources().getDimension(R.dimen.default_row_header_width));
        tableViewAdapter.setColumnHeaderHeight((int)getResources().getDimension(R.dimen.default_column_header_height));

        tableViewAdapter.setAllItems(mColumnHeaderList, mRowHeaderList , mCellList);
        tableViewAdapter.setAllITemsList(mColumnHeaderList, mRowHeaderList , mCellList);

        tableView.setAdapter(tableViewAdapter);

        tableView.setTableViewListener(new ITableViewListener() {
            @Override
            public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                if (row < mRowHeaderList.size() -1) {
                    Timber.d("onCellClicked" + String.valueOf(row) + String.valueOf(column));
                    Intent intent = new Intent(ExcelActivity.this, GraphActivity.class);
                    intent.putExtra(ValueConstants.SELECTED_GRAPH_ID, row);
                    startActivity(intent);
                }
            }

            @Override
            public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                if (column > 0 && column < mColumnHeaderList.size() - 1) {
                    if (sortState.equals(SortState.DESCENDING) || sortState.equals(SortState.UNSORTED) ) {
                        Timber.d("ascending bloack");
                        mCellList = excellPresenter.sortData(column, SortState.ASCENDING, mCellList);
                        sortState = SortState.ASCENDING;
                    }else {
                        Timber.d("DESCENDING bloack");
                        mCellList = excellPresenter.sortData(column, SortState.DESCENDING, mCellList);
                        sortState = SortState.DESCENDING;
                    }
                    tableViewAdapter.setCellItems(mCellList);
                    tableViewAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                DialogFragment popUpDialogFragment = new PopUpDialogFragment();
                popUpDialogFragment.show(ExcelActivity.this.getFragmentManager(), "PopUpDialogFragment");
            }

            @Override
            public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

            }

            @Override
            public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

            }
        });
    }

    private int columnHeaderLongPressAction(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.column_dialog_message);
        builder.setPositiveButton(R.string.column_dialog_action_total, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                action = ACTION_TOTAL;
            }
        });
        builder.setNegativeButton(R.string.column_dialog_action_graph, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                action = ACTION_GRAPH;
            }
        });

        builder.create();
        builder.show();

        return action;
    }
}
