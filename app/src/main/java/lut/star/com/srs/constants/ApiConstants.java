package lut.star.com.srs.constants;

/**
 * Created by kamrulhasan on 18/9/18.
 */
public interface ApiConstants {
    //API BASE url
    String BASE_URL = "https://demo4788203.mockable.io"; // live base url //"http://frugal.thisisbrite.com/"
    String HEADER_NAME_CONTENT = "Content-Type";
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_MULTIPART = "multipart/form-data";
}
