package lut.star.com.srs.uiModul.splash;

import android.content.Intent;
import android.os.Bundle;

import lut.star.com.srs.R;
import lut.star.com.srs.base.BaseActivity;
import lut.star.com.srs.uiModul.home.HomeActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }
}
