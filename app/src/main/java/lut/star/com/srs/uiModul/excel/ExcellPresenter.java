package lut.star.com.srs.uiModul.excel;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lut.star.com.srs.R;
import lut.star.com.srs.customViews.tableview.sort.SortState;
import lut.star.com.srs.datamodel.DataConvertedModel;
import lut.star.com.srs.datamodel.DataOne;
import lut.star.com.srs.datamodel.DataOneModel;
import lut.star.com.srs.uiModul.excel.adapter.model.Cell;
import lut.star.com.srs.webServiceModule.DataWebService;
import lut.star.com.srs.webServiceModule.WebServiceFactory;
import retrofit2.Response;
import timber.log.Timber;

public class ExcellPresenter {
    private Context context;
    private ExcelViewInterface viewInterface;
    private DataWebService webService;

    public ExcellPresenter(Context context, ExcelViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void getDataFromWebService(){
        webService = WebServiceFactory.createRetrofitService(DataWebService.class);

        webService.getAgendaListDay1()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DataOne>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<DataOne> dataOneResponse) {
                        if (dataOneResponse.isSuccessful()){
                            viewInterface.onDataFetched(dataOneResponse.body().dataOneModels);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getDataFromJson(){
        InputStream in = context.getResources().openRawResource(R.raw.xcelone);
        Reader reader = new BufferedReader(new InputStreamReader(in));
        Gson gson = new Gson();
        DataOne dataOne = gson.fromJson(reader, DataOne.class);

        viewInterface.onDataFetched(dataOne.dataOneModels);
        convertData(dataOne.dataOneModels);
    }

    public void convertData(List<DataOneModel> dataOneModels){
        List<DataConvertedModel>convertedModelList = new ArrayList<>();
        for (int i = 0 ; i < dataOneModels.size() ; i++){
            DataConvertedModel model = new DataConvertedModel();
            model.artikelBelong = dataOneModels.get(i).artikelBelong;
            model.openOrder = Double.parseDouble(dataOneModels.get(i).openOrder);
            model.zukunft = Double.parseDouble(dataOneModels.get(i).zukunft);
            model.nov17 = Double.parseDouble(dataOneModels.get(i).nov17);
            model.dez17 = Double.parseDouble(dataOneModels.get(i).dez17);
            model.jan18 = Double.parseDouble(dataOneModels.get(i).jan18);
            model.feb18 = Double.parseDouble(dataOneModels.get(i).feb18);
            model.mrz18 = Double.parseDouble(dataOneModels.get(i).mrz18);
            model.apr18 = Double.parseDouble(dataOneModels.get(i).apr18);
            model.mai18 = Double.parseDouble(dataOneModels.get(i).mai18);
            model.jun18 = Double.parseDouble(dataOneModels.get(i).jun18);
            model.jul18 = Double.parseDouble(dataOneModels.get(i).jul18);
            model.aug18 = Double.parseDouble(dataOneModels.get(i).aug18);
            model.total = Double.parseDouble(dataOneModels.get(i).total);

            convertedModelList.add( model );
        }

//        viewInterface.onDataFetched(convertedModelList);
    }

    public List<List<Cell>> sortData(int column, SortState sortState, List<List<Cell>> sortData){
        if (sortState.equals(SortState.ASCENDING)){
            for (int i = 0 ; i < sortData.size() ; i++){
                Timber.d(sortData.get(i).get(column).getData()+"");
            }
            for (int i = 0 ; i < sortData.size() ; i++){
                for (int j = 1 ; j < sortData.size()-i ; j++){
                    List<Cell> cellList = sortData.get(j);
                    List<Cell> cellList1 = sortData.get(j-1);
                    if (Double.parseDouble(String.valueOf(cellList1.get(column).getData())) > Double.parseDouble(String.valueOf(cellList.get(column).getData()))){
                        sortData.set(j-1, cellList);
                        sortData.set(j, cellList1);
                    }
                }
            }
            for (int i = 0 ; i < sortData.size() ; i++){
                Timber.d(sortData.get(i).get(column).getData()+"");
            }
            return sortData;
        }else {
            for (int i = 0 ; i < sortData.size() ; i++){
                Timber.d(sortData.get(i).get(column).getData()+"");
            }
            for (int i = 0 ; i < sortData.size() ; i++){
                for (int j = 1 ; j < sortData.size()-i ; j++){
                    List<Cell> cellList = sortData.get(j);
                    List<Cell> cellList1 = sortData.get(j-1);
                    if (Double.parseDouble(String.valueOf(cellList1.get(column).getData())) < Double.parseDouble(String.valueOf(cellList.get(column).getData()))){
                        sortData.set(j-1, cellList);
                        sortData.set(j, cellList1);
                    }
                }
            }
            for (int i = 0 ; i < sortData.size() ; i++){
                Timber.d(sortData.get(i).get(column).getData()+"");
            }
            return sortData;
        }
    }
}
