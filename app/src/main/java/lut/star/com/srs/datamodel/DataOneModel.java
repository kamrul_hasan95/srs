package lut.star.com.srs.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataOneModel {
    @SerializedName("Artikel_Belong")
    @Expose
    public String artikelBelong;
    @SerializedName("Open Order")
    @Expose
    public String openOrder;
    @SerializedName("Zukunft")
    @Expose
    public String zukunft;
    @SerializedName("Nov.17")
    @Expose
    public String nov17;
    @SerializedName("Dez.17")
    @Expose
    public String dez17;
    @SerializedName("Jan.18")
    @Expose
    public String jan18;
    @SerializedName("Feb.18")
    @Expose
    public String feb18;
    @SerializedName("Mrz.18")
    @Expose
    public String mrz18;
    @SerializedName("Apr.18")
    @Expose
    public String apr18;
    @SerializedName("Mai.18")
    @Expose
    public String mai18;
    @SerializedName("Jun.18")
    @Expose
    public String jun18;
    @SerializedName("Jul.18")
    @Expose
    public String jul18;
    @SerializedName("Aug.18")
    @Expose
    public String aug18;
    @SerializedName("Total")
    @Expose
    public String total;

    @Override
    public String toString() {
        return "DataOneModel{" +
                "artikelBelong='" + artikelBelong + '\'' +
                ", openOrder='" + openOrder + '\'' +
                ", zukunft='" + zukunft + '\'' +
                ", nov17='" + nov17 + '\'' +
                ", dez17='" + dez17 + '\'' +
                ", jan18='" + jan18 + '\'' +
                ", feb18='" + feb18 + '\'' +
                ", mrz18='" + mrz18 + '\'' +
                ", apr18='" + apr18 + '\'' +
                ", mai18='" + mai18 + '\'' +
                ", jun18='" + jun18 + '\'' +
                ", jul18='" + jul18 + '\'' +
                ", aug18='" + aug18 + '\'' +
                ", total='" + total + '\'' +
                '}';
    }
}
