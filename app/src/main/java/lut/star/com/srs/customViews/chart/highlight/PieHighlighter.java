package lut.star.com.srs.customViews.chart.highlight;

import lut.star.com.srs.customViews.chart.charts.PieChart;
import lut.star.com.srs.customViews.chart.data.Entry;
import lut.star.com.srs.customViews.chart.interfaces.datasets.IPieDataSet;

public class PieHighlighter extends PieRadarHighlighter<PieChart> {

    public PieHighlighter(PieChart chart) {
        super(chart);
    }

    @Override
    protected Highlight getClosestHighlight(int index, float x, float y) {

        IPieDataSet set = mChart.getData().getDataSet();

        final Entry entry = set.getEntryForIndex(index);

        return new Highlight(index, entry.getY(), x, y, 0, set.getAxisDependency());
    }
}
